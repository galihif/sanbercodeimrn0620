// Soal No. 1 (Array to Object)

function arrayToObject(arr) {
    
    for (let i = 0; i <arr.length; i++){
        let nomor = "";
        nomor+= i+1 + ". ";

        let nama = "";
        nama += arr[i][0] + " "+ arr[i][1] + " : ";

        let fn = arr[i][0];
        let ln = arr[i][1];
        let ge = arr[i][2];

        var now = new Date()
        var thisYear = now.getFullYear()
        let lenArr = arr[i].length;
        let umur = 0
        if (lenArr == 3 || arr[i][3]>thisYear){
            umur = "Invalid Birth Year"
        }
        else{
            umur = thisYear - arr[i][3];
        }

        let arrObj = {
            firstName: fn,
            lastName: ln,
            gender: ge,
            age: umur
        }
        process.stdout.write(nomor+nama)
        console.log(arrObj)
    }

}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)

console.log("============================================");

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    if (!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else if (money <50000){
        return "Mohon maaf, uang tidak cukup";
    }
    else{

        var moneyAwal = money;

        var lp = [];

        if (money >= 1500000){
            lp.push('Sepatu Stacattu');
            money -= 1500000;
        }
        if (money >= 500000){
            lp.push('Baju Zoro');
            money -= 500000
        }
        if (money >= 250000){
            lp.push('Baju H&N')
            money -= 250000;
        }
        if (money >= 175000){
            lp.push('Sweater Uniklooh');
            money -= 175000
        }
        if (money >= 50000){
            lp.push('Casing Handphone');
            money -= 50000
        }



        var data = {memberId: memberId, money: moneyAwal, listPurchased: lp, changeMoney: money };
        return data;
    }
  }

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("============================================");

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let total = []
    for (let i = 0; i <arrPenumpang.length; i++){
        let nama = arrPenumpang[i][0];
        let naik = arrPenumpang[i][1];
        let turun = arrPenumpang[i][2];
        let bayar = 0;

        let jarak = rute.indexOf(turun)-rute.indexOf(naik);

        bayar = 2000*jarak;

        let data = {penumpang: nama, naikDari: naik, tujuan: turun, bayar: bayar};
        total.push(data);
    }
    return total;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]