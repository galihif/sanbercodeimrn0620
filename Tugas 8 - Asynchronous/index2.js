var readBooksPromise = require('./promise.js');
const readBooks = require('./callback.js');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]


function mulai(index,times){
    if (index >= books.length){
        "";
    }
    else{
        readBooksPromise(times,books[index])
        .then (result => {
            times -= books[index].timeSpent;
            readBooksPromise(times,books[index+1])
            .then (result => {
                times -= books[index+1].timeSpent;
                readBooksPromise(times,books[index+2])
            })
        })
        
    }
}

mulai(0,10000)
