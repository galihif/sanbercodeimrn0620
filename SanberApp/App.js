import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import LoginUI from './Tugas/Tugas_13/LoginScreen';
import AboutUI from './Tugas/Tugas_13/AboutScreen'
import TodoUI from './Tugas/Tugas_14/App';
import SkillUI from './Tugas/Tugas_14/SkillScreen';
import ScreenUI from './Tugas/Tugas_15/index';
import NavUI from './Tugas/Tugas_Navigation/index';
import { ScrollView } from 'react-native-gesture-handler';
// import Quiz3 from '../Quiz 3/index'

export default function App() {
  return (
    <View style={{flex:1}}>
      <ScrollView>
        <LoginUI/>
        <AboutUI/>
        <SkillUI/>
        {/* <TodoUI/> */}
        {/* <ScreenUI/>
        <NavUI/>\ */}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
