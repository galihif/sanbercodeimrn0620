import React, { Component } from 'react';
import { Text, View } from 'react-native'; 
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from '../Tugas_13/LoginScreen';
import About from '../Tugas_13/AboutScreen';
import Skill from '../Tugas_14/SkillScreen';
import Project from './ProjectScreen';
import Add from './AddScreen';

const AuthStack = createStackNavigator();
const LoginStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const AboutDrawer = createDrawerNavigator();


const LoginStackScreen = () => (
    <LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={Login} options={{headerTitle: false}}/>
    </LoginStack.Navigator>
)

const AboutTabsScreen = () => (
    <Tabs.Navigator>
      <Tabs.Screen name="About" component={About}/>
      <Tabs.Screen name="Skills" component={Skill}/>
      <Tabs.Screen name="Add" component={Add}/>
      <Tabs.Screen name="Project" component={Project}/>
    </Tabs.Navigator>
  )


function App(){
    return(
        <NavigationContainer>
            <AuthStack.Navigator>
                <AuthStack.Screen name="Login" component={LoginStackScreen} options={{headerShown: false}}/>
                <AuthStack.Screen name="About" component={AboutTabsScreen} options={{}}/>
            </AuthStack.Navigator>
        </NavigationContainer>
    )
}

export default App