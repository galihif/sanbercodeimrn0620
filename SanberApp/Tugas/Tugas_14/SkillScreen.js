import React, {Component} from 'react';
import {View,Text, ImageBackground, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ProgressBar from 'react-native-progress/Bar';

import Login from '../Tugas_13/LoginScreen'

export const Skill = ({navigation}) => {
    return(
        <ImageBackground style={{flex:1}} source={require('./images/Home.png')}>
            <ScrollView>
                <View style={styles.menuContainer}>
                    <TouchableOpacity>
                        <Icon style={styles.menu} name="menu" color="white" size={50} />
                    </TouchableOpacity>
                    
                </View>

                <View style={styles.welcomeContainer}>
                    <Text style={styles.welcomeText}>Welcome,</Text>
                    <Text style={styles.welcomeText}>User</Text>
                </View>

                <View style={styles.skillsTextContainer}>
                    <Text style={styles.skillsText}>Skills</Text>
                </View>

                <View style={styles.skillsContainer}>
                    <View style={styles.skills}>
                        <Ionicons style={styles.skillsIcon} name="logo-python" size={40} color='#18B0F3'/>
                        <View style={styles.skillsDefContainer}>
                            <View style={styles.skillsTitleContainer}>
                                <Text style={styles.skillsName}>Python Basic  </Text>
                                <Text style={styles.skillsCat}>  Programming Lang</Text>
                            </View>
                            <View style={styles.skillsProgress}>
                                <ProgressBar style={styles.progress} progress={0.3} width={270} color='#FC6D26' />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skills}>
                        <Icons style={styles.skillsIcon} name="react" size={40} color='#18B0F3'/>
                        <View style={styles.skillsDefContainer}>
                            <View style={styles.skillsTitleContainer}>
                                <Text style={styles.skillsName}>React Native  </Text>
                                <Text style={styles.skillsCat}>  Framework</Text>
                            </View>
                            <View style={styles.skillsProgress}>
                                <ProgressBar style={styles.progress} progress={0.6} width={270} color='#FC6D26' />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skills}>
                        <Ionicons style={styles.skillsIcon} name="logo-javascript" size={40} color='#18B0F3'/>
                        <View style={styles.skillsDefContainer}>
                            <View style={styles.skillsTitleContainer}>
                                <Text style={styles.skillsName}>Python Basic  </Text>
                                <Text style={styles.skillsCat}>  Programming Lang</Text>
                            </View>
                            <View style={styles.skillsProgress}>
                                <ProgressBar style={styles.progress} progress={0.48} width={270} color='#FC6D26' />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skills}>
                        <Ionicons style={styles.skillsIcon} name="logo-github" size={40} color='#18B0F3'/>
                        <View style={styles.skillsDefContainer}>
                            <View style={styles.skillsTitleContainer}>
                                <Text style={styles.skillsName}>Github Intermediate  </Text>
                                <Text style={styles.skillsCat}>  Technology</Text>
                            </View>
                            <View style={styles.skillsProgress}>
                                <ProgressBar style={styles.progress} progress={0.78} width={270} color='#FC6D26' />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skills}>
                        <Ionicons style={styles.skillsIcon} name="logo-html5" size={40} color='#18B0F3'/>
                        <View style={styles.skillsDefContainer}>
                            <View style={styles.skillsTitleContainer}>
                                <Text style={styles.skillsName}>HTML Advanced  </Text>
                                <Text style={styles.skillsCat}>  Technology </Text>
                            </View>
                            <View style={styles.skillsProgress}>
                                <ProgressBar style={styles.progress} progress={0.4} width={270} color='#FC6D26' />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skills}>
                        <Ionicons style={styles.skillsIcon} name="logo-npm" size={40} color='#18B0F3'/>
                        <View style={styles.skillsDefContainer}>
                            <View style={styles.skillsTitleContainer}>
                                <Text style={styles.skillsName}>NPM Basic  </Text>
                                <Text style={styles.skillsCat}>  Technology </Text>
                            </View>
                            <View style={styles.skillsProgress}>
                                <ProgressBar style={styles.progress} progress={0.8} width={270} color='#FC6D26' />
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </ImageBackground>

    )
    
        
}

export default Skill

const styles = StyleSheet.create({
    menuContainer: {
        flexDirection: 'row',
        justifyContent:'flex-start',
        marginBottom: 30
    },
    menu: {
        marginHorizontal: 25,
        marginTop: 40
    },
    welcomeContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        marginBottom: 20
    },
    welcomeText: {
        // fontFamily: 'coolvetica',
        fontSize: 36,
        color: 'white',
        fontWeight: 'bold',
        marginHorizontal: 30
    },
    skillsTextContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: 20,
        marginBottom: 20
    },
    skillsText: {
        // fontFamily: 'coolvetica',
        fontSize: 28,
        color: 'white',
        fontWeight: 'bold',
        marginHorizontal: 30
    },
    skillsContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    skills: {
        width: 360,
        height: 48,
        backgroundColor: 'white',
        borderRadius: 10,
        marginVertical: 4,
        borderColor: '#FFBD4A',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#FFBD4A'
    },
    skillsIcon: {
        marginLeft: 12
    },
    skillsDefContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        marginLeft: 12
    },
    skillsTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    skillsProgress: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center'
    },
    skillsName: {
        fontSize: 15,
        color: '#666666',
        marginTop: 2,
        fontWeight: 'bold'
    },
    skillsCat: {
        fontSize: 10,
        color: 'grey',
        marginTop: 4
    },
    progress: {
        marginTop: 7
    }
})

