import React, {Component} from 'react';
import {} from 'react-navigation'
import {View,
        Text,
        StyleSheet,
        Image,
        TouchableOpacity,
        FlatList,
        Button,
        ImageBackground
    } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';

export const About = ({navigation}) => {
    return (

        <ImageBackground style={{flex:1}} source={require('./images/About.png')}>

            <View style={{flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                <Text style={styles.headerText}>About Me</Text>
                <Image source={require('./images/profil.jpg')} style={styles.profilImage} />
                <Text style={styles.nameText}>Galih Indra Firmansyah</Text>
                <Text style={styles.jobText}>React Native Developer</Text>
                <Text style={styles.smText}>Social Media</Text>
                <View style={styles.smIconContainer}>
                    <TouchableOpacity>
                        <IonIcon name='logo-facebook' color='#18B0F3' size={60} style={styles.smIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <IonIcon name='logo-instagram' color='#18B0F3' size={60} style={styles.smIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <IonIcon name='logo-twitter' color='#18B0F3' size={54} style={styles.smIcon}/>
                    </TouchableOpacity>
                </View>
                <Text style={styles.smText}>Projects</Text>
                <View style={styles.smIconContainer}>
                    <TouchableOpacity>
                        <IonIcon name='logo-github' color='#18B0F3' size={60} style={styles.smIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <IonIcon name='logo-github' color='#18B0F3' size={60} style={styles.smIcon}/>
                    </TouchableOpacity>
                </View>
            </View>

            

        </ImageBackground>
        
    )
}

export default About

const styles = StyleSheet.create({
    headerText: {
        fontSize: 35,
        color: 'white',
        fontWeight: 'bold',
        marginTop: 70
    },
    profilImage: {
        width: 150,
        height: 150,
        borderRadius: 120,
        marginTop: 35
    },
    nameText: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 14
    },
    jobText: {
        fontSize: 14,
        color: '#666666',
        marginTop: 4
    },
    smText: {
        fontSize: 22,
        fontWeight: 'bold',
        marginTop: 22
    },
    smIconContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    smIcon: {
        margin: 10
    }


   
})