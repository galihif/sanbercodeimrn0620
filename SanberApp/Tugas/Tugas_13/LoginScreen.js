import React, {Component, useState} from 'react';
import {} from 'react-navigation'
import {View,
        Text,
        StyleSheet,
        Image,
        TouchableOpacity,
        FlatList,
        Button,
        ImageBackground
    } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import About from './AboutScreen'
import { TextInput } from 'react-native-gesture-handler';

export const Login = ({navigation}) => {
    const [name,setName] = useState('User');
    return (
        <ImageBackground style={{flex:1}} source={require('./images/Login.png')}>
            <View style={styles.logoContainer}>
                <Image source={require('./images/dl1.png')} style={styles.logo} />
            </View>

            <View style={styles.inputContainer}>
                <TextInput style={styles.inputForm} placeholder='Name' placeholderTextColor='#666666'/>
                <TextInput style={styles.inputForm} placeholder='Email' placeholderTextColor='#666666'/>
                <TextInput style={styles.inputForm} placeholder='Password' placeholderTextColor='#666666' secureTextEntry={true}/>
                <TextInput style={styles.inputForm} placeholder='Confirm Password' placeholderTextColor='#666666' secureTextEntry={true} />
            </View>

            <View style={styles.buttonContainer}>
                <Button title='SIGN UP' onPress={() => navigation.push('About')}/>
                <Text style={styles.alreadyText}>Already have an account?</Text>
                <Button title='LOGIN'/>

            </View>
        </ImageBackground>
    )
}

export default Login;

const styles = StyleSheet.create({

    logoContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    logo: {
        marginTop: 100
    },
    inputContainer: {
        flexDirection: 'column',
        justifyContent:'flex-start',
        alignItems: 'center',
        marginTop: 20
    },
    inputForm: {
        borderBottomWidth: 1,
        borderBottomColor: '#666666',
        width:280,
        height: 40,
        marginTop:10
    },
    buttonContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40
    },
    alreadyText: {
        marginVertical: 15
    }

})