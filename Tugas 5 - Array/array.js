// SOAL 1 RANGE

function range(x,y){
    if (x<y){
        arr = new Array(0);
        for (var i = x; i <= y; i++){
            arr.push(i);
        }
        return arr;
    }
    else if (x>y){
        arr = new Array(0);
        for (var i = x; i >= y; i--){
            arr.push(i);
        }
        return arr;
    }
    else if (!x||!y){
        return -1;
    }
}

console.log(range(54,50));

console.log("==============================");

// SOAL 2 RANGE WITH STEP

function rangeWithStep(x,y,z){
    arr = new Array(0);
    if (x<y){
        for (var i = x; i<=y; i+=z){
            arr.push(i);
        }
        return arr;
    }
    else if (x>y){
        for (var i = x; i >= y; i-=z){
            arr.push(i);
        }
        return arr;
    }
}

console.log(rangeWithStep(29,2,4));

console.log("==============================");

// SOAL 3 SUM OF RANGE

function sum(x,y,z){
    var jml = 0;
    if (!x &&!y){
        return 0;
    }
    else if (!x||!y){
        return x;
    }
    else if (!z){
        if (x<y){
            for (var i = x; i<=y; i+=1){
                jml+=i;
            }
            return jml;
        }
        else if (x>y){
            for (var i = x; i >= y; i-=1){
                jml+=i;
            }
            return jml;
        }
    }
    else {
        if (x<y){
            for (var i = x; i<=y; i+=z){
                jml+=i;
            }
            return jml;
        }
        else if (x>y){
            for (var i = x; i >= y; i-=z){
                jml+=i;
            }
            return jml;
        }
    }
}

console.log(sum(20,10,2));

console.log("==============================");

// SOAL 4 ARRAY MULTIDIMENSI

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataArray(arr,n){
    console.log("Nomor ID: " + arr[n][0]);
    console.log("Nama: " + arr[n][1]);
    console.log("TTL: " + arr[n][2] + " " + arr[n][3]);
    console.log("Hobi: " + arr[n][4]);
}

function dataHandling(arr){
    var j = arr.length;
    for (var i = 0; i < j; i++){
        dataArray(arr,i);
        console.log("")
    }
    
}

dataHandling(input);

console.log("==============================");

//  SOAL 5 BALIK KATA

function balikKata(str){
    var kata = "";
    var panjangStr = str.length;
    for (i = panjangStr-1; i >= 0; i--){
        kata+=str[i];
    }

    return kata;
}

console.log(balikKata("I am Sanbers"));

console.log("==============================");

// SOAL 6 METODE ARRAY

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2 (arr){
    arr[1] = arr[1].split(" ")
    arr[1].push("Elsharawy");
    arr[1] = arr[1].join(" ");

    arr[2] = arr[2].split(" ");
    arr[2].unshift("Provinsi");
    arr[2] = arr[2].join(" ");

    arr.splice(4,1,"Pria","SMA Internasional Metro")

    var tgl = arr[3];
    tgl = tgl.split("/");
    var bulan = tgl[1];

    var tgl2 = [tgl[2],tgl[0],tgl[1]];

    tgl3 = tgl.join("-");

    nama = arr[1];
    nama = nama.split(" ");
    nama.pop();
    nama = nama.join(" ");
    
    console.log(arr);
    switch(bulan){
        case "01": {
            console.log("Januari");
            break;
        }
        case "02": {
            console.log("Februari");
            break;
        }
        case "03": {
            console.log("Maret");
            break;
        }
        case "04": {
            console.log("April");
            break;
        }
        case "05": {
            console.log("Mei");
            break;
        }
        case "06": {
            console.log("Juni");
            break;
        }
        case "07": {
            console.log("Juli");
            break;
        }
        case "08": {
            console.log("Agustus");
            break;
        }
        case "09": {
            console.log("September");
            break;
        }
        case "10": {
            console.log("Oktober");
            break;
        }
        case "11": {
            console.log("November");
            break;
        }
        case "12": {
            console.log("Desember");
            break;
        }
    }
    console.log(tgl2);
    console.log(tgl3);
    console.log(nama);

}

dataHandling2(input);