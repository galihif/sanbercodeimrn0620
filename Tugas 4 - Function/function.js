// No 1

function teriak(){
    return "Halo Sanbers!";
}

console.log(teriak());

console.log("=============================================");

// No 2

function kalikan(num1,num2){
    return num1*num2;
}

var n1 = 12;
var n2 = 28;
hasilkali = kalikan(n1,n2);
console.log(hasilkali);

console.log("=============================================");

// No 3

function introduce(name,age,address,hobby){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby;
}

var nama = "Galih";
var umur = 19;
var alamat = "Pogung Rejo";
var hobi = "Coding";

var perkenalan = introduce(nama,umur,alamat,hobi);

console.log(perkenalan);