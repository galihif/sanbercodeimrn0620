// LOOPING WHILE
console.log("LOOPING PERTAMA");
var l1 = 1;
while(l1 <= 20){
    if (l1%2==0){
        console.log(l1+" - I love coding");
    }
    l1++;
}

console.log("LOOPING KEDUA");
var l2 = 20;
while (l2>0){
    if (l2%2==0){
        console.log(l2+" - I will become a mobile developer");
    }
    l2--;
}


// LOOPING FOR
console.log("OUTPUT");
for (var i = 1; i<=20; i++){
    if (i%3 == 0 && i%2 != 0){
        console.log(i+" - I Love Coding");
    }
    else if (i%2 == 0){
        console.log(i+" - Berkualitas");
    }
    else{
        console.log(i+" - Santai");
    }
}

// PERSEGI PANJANG
console.log("PERSEGI PANJANG");
for (var i = 1; i <= 4; i++){
    console.log("########");
}

// TANGGA
console.log("TANGGA")
for (var i = 1; i <=7; i++){
    switch(i){
        case 1: {
            console.log("#");
            break;
        }
        case 2: {
            console.log("##");
            break;
        }
        case 3: {
            console.log("###");
            break;
        }
        case 4: {
            console.log("####");
            break;
        }
        case 5: {
            console.log("#####");
            break;
        }
        case 6: {
            console.log("######");
            break;
        }
        case 7: {
            console.log("#######");
            break;
        }
    }
}

// PAPAN CATUR
console.log("PAPAN CATUR");
for (var i = 1; i <= 8; i++){
    if (i%2 == 0){
        console.log("# # # # ");
    }
    else {
        console.log(" # # # #");
    }
}